# MProv Sample Streaming Client
## Z. Ives, 11/2018

This sample code actually includes two, largely parallel, sample clients.  One is in Java and the other in Python.

### Requirements

* Docker (modern versions also have Docker-Compose)

Java version:

* Java JDK, version 11 preferred, although Java 9 will work as well.
* Apache Maven for dependency management and building.

Python version:

* Python 3.6+

### Docker container

From the project directory, install the Docker containers, which use the provided `docker-compose.yaml`, as follows:

```
docker-compose up
```

This should set up three containers: (1) Neo4J, (2) PostgreSQL, (3) the PennProvenance container.

### Getting the sample data

We use the mHealthUB Cerebral Cortex 2.0 dataset at https://mhealth.md2k.org/resources/datasets.html.  To download it, run:

```
./download.sh
```

### Creating a user for the MProv database

Next you'll need to create a sample user.  Go to `http://localhost:8088` and click **Google log in**.  To directly use the sample code, you'll need to sign in with Google and add a local password.

### Building the demo

#### Python demo

Make sure you 

```
pip install pennprov

pip install -e .
```

Then you can run 

```
cd mprov
python mprov_example.py username password
```
where `username` is your username and `password` is the local password you created when logging in with Google.

which should start streaming data from the `test-data` directory.

##### Core logic: Python

The most relevant source modules for understanding provenance tracking are:

* `mprov_example` which is the main code.  You'll see it creates two operators, polls for output from the first, processes it through the second, and polls for output from that.
* `mprov_connection` which provides high-level APIs for writing PROV-DM.
* `csv_stream_source` which reads streaming data from the provided CSV files.
* `windowed_agg` which computes a sliding window and computes a lambda function over one field from the tuples in the window.

#### Java demo

To build the demo, you will need to go to the base directory and run

```
mvn install
```

which should pull all dependencies and build the main client.  You can then run:

```
java -jar target/mprov-minimal-0.0.1-SNAPSHOT.jar username password
```
where `username` is your username and `password` is the local password you created when logging in with Google.

##### Core logic: Java

The most relevant source modules for understanding provenance tracking are:

* `MProvConnection` is a higher-level set of libraries for writing PROV-DM with the appropriate node names, etc. that make sense for the MProv application.
* `CSVStreamSource` takes Cerebral Cortex-provided CSV values and generates streams.  The `writeProvenance` method is where it interfaces with `MProvConnection`.
* `WindowedAggregateOp` takes an input stream of data and produces an output stream, recording provenance.  Again, the `writeProvenance` method is where it interfaces with `MProvConnection`.  

The main program is `MProvExample` and the class `RegisterStreams` traverses the Cerebral Cortex files to register them as a set of stream sources.