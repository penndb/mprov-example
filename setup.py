from distutils.core import setup

setup(
    name='MProvExample',
    version='0.1dev',
    packages=['mprov',],
    author="Z. Ives",
    author_email="zives@cis.upenn.edu",
    license='Apache License, Version 2.0',
    long_description=open('LICENSE.txt').read(),
    install_requires=[
        "pennprov>=2.2.2"
    ]
)