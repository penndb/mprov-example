/************************************************
 * Copyright 2018 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.umemphis.ccortex;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;

/**
 * Registers sample data streams to make them available
 * 
 * @author ZacharyIves
 *
 */
public class RegisterStreams {
	
	/**
	 * Takes a set of stream files, as from the Cerebral Cortex sample data at
	 * https://mhealth.md2k.org/resources/datasets.html and returns a map
	 * from stream name to "source descriptor"
	 * 
	 * @param base
	 * @return
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public static Map<String,SourceDescriptor> sortSensors(Path base) throws JsonProcessingException, IOException {
		
		Map<String,SourceDescriptor> ret = new HashMap<>();
		
		File[] owners = base.toFile().listFiles(File::isDirectory);
		
		ObjectMapper mapper = new ObjectMapper();
		
		// This is a UUID for the owner, I guess
		for (File subdir: owners) {
			for (File date: subdir.listFiles(File::isDirectory)) {
				for (File sensor: date.listFiles(File::isDirectory)) {
					
					String[] csvs = sensor.list(new FilenameFilter() {
						@Override
						public boolean accept(File current, String name) {
							return name.endsWith(".gz");
						}
					}) ;
					File[] jsons = sensor.listFiles(new FilenameFilter() {
						@Override
						public boolean accept(File current, String name) {
							return name.endsWith(".json");
						}
					}) ;
					File[] gzs = sensor.listFiles(new FilenameFilter() {
						@Override
						public boolean accept(File current, String name) {
							return name.endsWith(".gz");
						}
					}) ;
					JsonNode root = mapper.readTree(jsons[0]);
					
					JsonNode schema = root.get("data_descriptor");
					JsonNode metadata = root.get("execution_context");
					
					List<String> names = new ArrayList<>();
					List<String> types = new ArrayList<>();
					double frequency = Double.MAX_VALUE;
					if (schema.isArray()) {
						for (final JsonNode field: schema) {
							names.add(field.get("NAME").asText());
							types.add(field.get("DATA_TYPE").asText());
							
							if (field.get("FREQUENCY") != null) {
								double freq = field.get("FREQUENCY").asDouble();
								if (frequency > freq)
									frequency = freq;
							}
						}
					}
					
					String identifier = root.get("identifier").asText();
					String name = root.get("name").asText();
					
					SourceDescriptor descriptor = new SourceDescriptor(
							name, names, types, mapper.writeValueAsString(metadata), 
							identifier, frequency, gzs);
					System.out.println("Sensor " + descriptor.getSchema() + ": " + Lists.asList(csvs[0],csvs));
					
					ret.put(name, descriptor);
				}
			}
		}
		return ret;
	}

}
