/************************************************
 * Copyright 2018 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.umemphis.ccortex;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import edu.upenn.cis.db.habitat.core.type.BasicSchema;

public class SourceDescriptor implements Serializable {
	List<String> names;
	List<String> typeNames;
	String metadata;
	String sourceName;
	String identifier;
	double frequency;
	File[] files;
	
	BasicSchema theSchema = null;
	
	public SourceDescriptor(String sourceName, List<String> fields, List<String> types,
			String metadata, String identifier, double frequency, File[] files) {
		this.sourceName = sourceName;
		this.names = fields;
		this.typeNames = types;
		this.metadata = metadata;
		this.files = files;
		this.identifier = identifier;
		this.frequency = frequency;
	}
	
	public double getFrequency() {
		return frequency;
	}
	
	public String getName() {
		return sourceName;
	}
	
	public File[] getFiles() {
		return files;
	}
	
	public File getFile(int inx) {
		return files[inx];
	}
	
	public String getSourceIdentifier() {
		return identifier;
	}

	public BasicSchema getSchema() {
		if (theSchema != null)
			return theSchema;
		
		List<Class<?>> types = new ArrayList<>();
		
		for (String tName : typeNames) {
			if (tName.toLowerCase().equals("double") || tName.toLowerCase().equals("float"))
				types.add(Double.class);
			else if (tName.toLowerCase().equals("int") || tName.toLowerCase().equals("integer"))
				types.add(Integer.class);
			else if (tName.toLowerCase().equals("string"))
				types.add(String.class);
			else if (tName.toLowerCase().equals("doublearray"))
				types.add(String.class);
			else
				throw new IllegalArgumentException("Can't determine type " + tName);
		}
		
		names.add(0, "rid");
		types.add(0, Integer.class);
		names.add(1, "timestamp");
		types.add(1, Long.class);
		names.add(2, "offset");
		types.add(2, Integer.class);
		theSchema = new BasicSchema(sourceName, names, types);
		return theSchema;
	}
	
	public List<String> getFieldNames() {
		return names;
	}

	public void setName(String sourceName) {
		this.sourceName = sourceName;
	}
}
