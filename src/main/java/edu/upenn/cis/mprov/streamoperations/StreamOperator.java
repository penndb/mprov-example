/************************************************
 * Copyright 2018 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.mprov.streamoperations;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.mprov.client.MProvConnection;

public abstract class StreamOperator implements IStreamSink, IStreamSource {
	protected List<BasicSchema> inputSchemas;
	protected BasicSchema outputSchema;
	protected MProvConnection provenance;
	String name;
	
	protected Queue<BasicTuple> outputs = new LinkedList<>();
	
	public StreamOperator(String name, 
			MProvConnection provenance) {
		this.name = name;
		this.provenance = provenance;
	}

	@Override
	public List<BasicTuple> poll(int max) {
		List<BasicTuple> ret = new ArrayList<>();
		
		for (int i = 0; i < max; i++) {
			BasicTuple val = outputs.poll();
			 
			if (val != null)
				ret.add(val);
		}
		
		return ret;
	}

	@Override
	public void initialize(List<BasicSchema> inputStreams) {
		inputSchemas = inputStreams;
	}
	
	@Override
	public void initialize() {
	}

	public String getName() {
		return name;
	}
}
