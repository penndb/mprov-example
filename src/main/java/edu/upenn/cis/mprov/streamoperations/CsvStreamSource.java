/************************************************
 * Copyright 2018 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.mprov.streamoperations;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.GZIPInputStream;

import edu.umemphis.ccortex.SourceDescriptor;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.BooleanField;
import edu.upenn.cis.db.habitat.core.type.DoubleField;
import edu.upenn.cis.db.habitat.core.type.Field;
import edu.upenn.cis.db.habitat.core.type.IntField;
import edu.upenn.cis.db.habitat.core.type.LongField;
import edu.upenn.cis.db.habitat.core.type.ProvStringToken;
import edu.upenn.cis.db.habitat.core.type.ProvToken;
import edu.upenn.cis.db.habitat.core.type.StringField;
import edu.upenn.cis.db.habitat.core.type.provdm.QualifiedName;
import edu.upenn.cis.mprov.client.MProvConnection;

public class CsvStreamSource extends StreamSource {
	SourceDescriptor source;
	List<String> fieldNames;
	List<Field<?>> fieldValues;
	
	BufferedReader reader;
	int cursor = 0;
	int count = 0;
	boolean processedOnce = false;
	final static int LAST = 10000;
	
	public CsvStreamSource(
			String name, 
			SourceDescriptor source, 
			List<String> fieldNames, 
			List<Field<?>> values, 
			MProvConnection provenance) {
		super(name, provenance);
		
		this.source = source;
		
		this.schema = source.getSchema();
		schema.addField("streamid", String.class);
		schema.addField("_prov", String.class);
		
		if (fieldNames != null)
			for (int i = 0; i < fieldNames.size(); i++) {
				schema.addField(fieldNames.get(i), values.get(i).getType());
			}
		
		this.fieldNames = fieldNames;
		this.fieldValues = values;
	}
	
	@Override
	public void initialize() {
		try {
			reader = new BufferedReader( 
					new InputStreamReader(new GZIPInputStream(new FileInputStream(source.getFile(cursor)))));
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	void delayTime() {
		long delay = (long)(1000.0 / source.getFrequency());
		try {
			Thread.sleep(delay);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	@Override
	public List<BasicTuple> poll(int max) {
		String strLine = "";
		
		List<BasicTuple> ret = new ArrayList<>();
		
		// OPTIONAL:
		// Sleep so it's in real-time
		delayTime();

		//reading records from file
		try {
			boolean read = false;
			do {
				read = false;
				if( (strLine = reader.readLine()) != null && (getTupleIndex() < LAST || LAST < 0)) {
					read = true;
					
					// creating a list of values (for tuple) having the attributes and
					// data of that entity
					List<Field<? extends Object>> valueFields = new LinkedList<>();
					IntField tuple_id_val = new IntField(getTupleIndex());
	
					String[] values = strLine.split(",");
					
					valueFields.add(tuple_id_val);
					
					// We are skipping the 0th item, which is a tuple ID
					// and the last item, which is provenance
					for (int i = 1; i < schema.getArity() - 2 - fieldValues.size(); i++) {
						if (i-1 >= values.length)
							throw new RuntimeException("Out of schema bounds");
						if (schema.getTypeAt(i) == Boolean.class) {
							valueFields.add(new BooleanField(Boolean.valueOf(values[i-1])));
						} else if (schema.getTypeAt(i) == Integer.class) {
							try {
								valueFields.add(new IntField(Integer.valueOf(values[i-1])));
							} catch (NumberFormatException nf) {
								valueFields.add(new IntField((Double.valueOf(values[i-1])).intValue()));
							}
						} else if (schema.getTypeAt(i) == String.class) {
							valueFields.add(new StringField(values[i-1]));
						} else if (schema.getTypeAt(i) == Double.class) {
							valueFields.add(new DoubleField(Double.valueOf(values[i-1])));
						} else if (schema.getTypeAt(i) == Long.class) {
							valueFields.add(new LongField(Long.valueOf(values[i-1])));
						} else 
							throw new IllegalArgumentException("Can't handle type " + schema.getTypeAt(i).getName());
					}
					
					valueFields.addAll(fieldValues);
					
					valueFields.add(new StringField(getName()));//source.getSourceIdentifier()));
					// Default for provenance
					valueFields.add(new StringField(""));
					
					// creating tuple from schema and setting the values
					BasicTuple tuple = schema.createTuple();
					tuple.setFields(valueFields);
					
					writeProvenance(tuple);
					ret.add(tuple);
	
					processedOnce = true;
				}
				if (!read) {
					reader.close();
					cursor++;
					// Wrap
					if (cursor >= source.getFiles().length)
						cursor = 0;
					reader = new BufferedReader( 
							new InputStreamReader(new GZIPInputStream(new FileInputStream(source.getFile(cursor) ))));
				}
			} while (!read && (getTupleIndex() < LAST || LAST < 0));
		} catch (IOException e) {
			e.printStackTrace();
		}
		// TODO Auto-generated method stub
		return ret;
	}
	
	void writeProvenance(BasicTuple tuple) {
		ProvToken token = new ProvStringToken(name + (getTupleIndex() - 1));
		tuple.setValue("_prov", name + (getTupleIndex() - 1));

		provenance.storeTuple(token, tuple, getTupleIndex() - 1);
		
		// If there is a location, store this as an *annotation* attached
		// to the tuple
		if (source.getFieldNames().contains("Location Name")) {
			String sourceToken = name + (getTupleIndex() - 1);
			String annToken = "x" + sourceToken;
			
			ProvToken sToken = new ProvStringToken(sourceToken);
			ProvToken aToken = new ProvStringToken(annToken);
			provenance.storeAnnotation(sToken, aToken, 
					new QualifiedName("P", "sensor"), tuple.getValue("Location Name").toString());
		}

	}

	@Override
	public void close() {
		// TODO Auto-generated method stub

		try {
			reader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected int getTupleIndex() {
		return count;
	}
	
	protected void incTupleIndex() {
		count++;
	}
}
