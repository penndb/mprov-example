/************************************************
 * Copyright 2018 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.mprov.streamoperations;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.OptionalDouble;
import java.util.OptionalLong;
import java.util.Queue;
import java.util.function.Function;

import edu.upenn.cis.db.habitat.core.type.BasicSchema;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.ProvLocation;
import edu.upenn.cis.db.habitat.core.type.ProvSpecifier;
import edu.upenn.cis.db.habitat.core.type.ProvStringToken;
import edu.upenn.cis.db.habitat.core.type.ProvToken;
import edu.upenn.cis.mprov.client.MProvConnection;

/**
 * Core stream operator for taking a sliding window
 * and applying a user-specified function over it.
 * Currently very simple (doesn't do grouping) but
 * captures the basics.
 * 
 * @author ZacharyIves
 *
 * @param <T>
 * @param <R>
 */
public class WindowedAggregateOp<T,R> extends StreamOperator {
	BasicSchema outputSchema;
	Map<String,Queue<BasicTuple>> inputs = new HashMap<>();
	int windowSize;
	int slide;
	String aggField;
	Function<List<T>,R> aggFunction;
	Class<R> aggClass;
	Class<T> resultClass;
	int windowId = 0;
	
	/**
	 * Create a streaming windowed agg operator
	 * 
	 * @param name Operator name
	 * @param windowSize Sliding window size
	 * @param slide How many tuples to slide by
	 * @param aggField Field to aggregate
	 * @param aggFunction Function to apply to the aggregate (may produce a different type)
	 * @param aggClass Base class of what's being aggregated
	 * @param resultClass Base class of the output
	 * @param provenance Provenance connection
	 */
	public WindowedAggregateOp(String name, int windowSize, int slide, String aggField, Function<List<T>,R> aggFunction, 
			Class<R> aggClass, Class<T> resultClass, 
			MProvConnection provenance) {
		super(name, provenance);
		this.windowSize = windowSize;
		this.slide = slide;
		this.aggField = aggField;
		this.aggFunction = aggFunction;
		this.aggClass = aggClass;
		outputSchema = new BasicSchema(name);
		outputSchema.addField(aggField + windowSize, resultClass);
	}

	@Override
	public void initialize(List<BasicSchema> inputStreams) {
		super.initialize(inputStreams);

	}

	@Override
	public void process(String source, BasicTuple tuple) {
		if (!inputs.containsKey(source))
			inputs.put(source, new LinkedList<>());
		inputs.get(source).add(tuple);
		
		if (inputs.get(source).size() == windowSize) {
			writeProvenance(inputs.get(source));
			List<T> projection = new ArrayList<>();
			for (BasicTuple t: inputs.get(source)) {
				projection.add((T)t.getValue(aggField));
			}
			for (int i = 0; i < slide; i++)
				inputs.get(source).remove();
					
			R result = aggFunction.apply(projection);
			
			BasicTuple aggResult = outputSchema.createTuple();
			if (result instanceof OptionalDouble) {
				OptionalDouble od = (OptionalDouble)result;
				if (od.isPresent()) {
					aggResult.setValueAt(0, od.getAsDouble());
					outputs.add(aggResult);
				}
			} else if (result instanceof OptionalLong) {
				OptionalLong ol = (OptionalLong)result;
				if (ol.isPresent()) {
					aggResult.setValueAt(0, ol.getAsLong());
					outputs.add(aggResult);
				}
			}
			windowId++;
		}
	}
	
	void writeProvenance(Collection<BasicTuple> inputWindow) {
		ProvStringToken activityToken = new ProvStringToken(provenance.getActivityID(getName(), windowId));
		//creating window token and storing the link between entity token and window token
		ProvToken windowToken = new ProvStringToken(provenance.getWindowID(getName(), windowId));
	  	
		//iterating through values in the input tuple window
		List<BasicTuple> list = new ArrayList<>();
		list.addAll(inputWindow);

		List<Integer> locationIntList = new ArrayList<>();
		locationIntList.add((Integer)list.get(0).getValue("rid"));
		locationIntList.add((Integer)list.get(list.size()-1).getValue("rid"));
		ProvSpecifier location = new ProvLocation("windowIdx", locationIntList);

		Date start = new Date();

		provenance.storeActivity(activityToken, start, start, location);

		List<ProvToken> tokenList = new ArrayList<>();
		for(BasicTuple entity: list) {
			// 		//creating token of each tuple value and adding it to the list
			ProvToken token = new ProvStringToken((String)entity.getValue("_prov"));//(String)"i"+entity.getValueByField("id")) ;
			tokenList.add(token);
		}

		provenance.storeWindowAndInputs(windowToken, tokenList, location);
	}

	@Override
	public void close() {
		// TODO Auto-generated method stub

	}

}
