/************************************************
 * Copyright 2018 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.mprov.client;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import com.mashape.unirest.http.exceptions.UnirestException;

import edu.upenn.cis.db.habitat.client.ProvDmClient;
import edu.upenn.cis.db.habitat.core.api.ProvDmApi;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.BooleanField;
import edu.upenn.cis.db.habitat.core.type.DoubleField;
import edu.upenn.cis.db.habitat.core.type.Field;
import edu.upenn.cis.db.habitat.core.type.IntField;
import edu.upenn.cis.db.habitat.core.type.LongField;
import edu.upenn.cis.db.habitat.core.type.ProvLocation;
import edu.upenn.cis.db.habitat.core.type.ProvSpecifier;
import edu.upenn.cis.db.habitat.core.type.ProvStringToken;
import edu.upenn.cis.db.habitat.core.type.ProvToken;
import edu.upenn.cis.db.habitat.core.type.StringField;
import edu.upenn.cis.db.habitat.core.type.TupleWithSchema;
import edu.upenn.cis.db.habitat.core.type.model.ProvSpecifierModel;
import edu.upenn.cis.db.habitat.core.type.provdm.Attribute;
import edu.upenn.cis.db.habitat.core.type.provdm.QualifiedName;
import edu.upenn.cis.db.habitat.core.type.provdm.StringAttribute;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;

/**
 * High-level interfaces between mProv and the PennProvenance store.
 * Many of these will ultimately move INTO the server-side.
 * 
 * @author ZacharyIves
 *
 */
public class MProvConnection {
	ProvDmClient prov;
	
	/**
	 * Establish a connection
	 * 
	 * @param user
	 * @param password
	 * @throws HabitatServiceException
	 * @throws UnirestException
	 */
	public MProvConnection(String user, String password) throws HabitatServiceException, UnirestException {
		// Set up
		
		prov = new ProvDmClient(user, password, "localhost", 8088);
		
		prov.createOrResetProvenanceGraph(getGraph());
	}
	
	/**
	 * Within the storage system, what is the graph name?
	 * 
	 * @return
	 */
	String getGraph() {
		return "mProv-graph";
	}

	/**
	 * Prov-DM storage API
	 * 
	 * @return
	 */
	public ProvDmApi getProvenanceStore() {
		return prov;
	}

	/**
	 * Create a unique ID for an operator stream window
	 * 
	 * @param operator
	 * @param id
	 * @return
	 */
	public static String getWindowID(String operator, int id) {
		return operator + "_w." + id;
	}
	

	/**
	 * Create a unique ID for a stream operator result
	 * 
	 * @param operator
	 * @param id
	 * @return
	 */
	public static String getResultID(String operator, int id) {
		return operator + "_r." + id;
	}

	/**
	 * Create a unique ID for an entity
	 * 
	 * @param operator
	 * @param id
	 * @return
	 */
	public static String getEntityID(String operator, int id) {
		return "e." + id;
	}
	
	/**
	 * Create a unique ID for an activity (a stream operator call)
	 * 
	 * @param operator
	 * @param id
	 * @return
	 */
	public static String getActivityID(String operator, int id) {
		return operator + "_a." + id;
	}
	
	/**
	 * Create a node for the activity
	 * 
	 * @param activityToken
	 * @param start
	 * @param end
	 * @param location
	 */
	public void storeActivity(ProvToken activityToken, Date start, Date end,
			ProvSpecifier location) {
		// storing activity token
		try {
			prov.activity(getGraph(), new QualifiedName("p", activityToken.toString()), start, end,
					ProvSpecifierModel.from(location));
			// prov.storeProvenanceNode(getGraph(), activityToken, tuple, location);
			// logger.info("node, activity, "+activity_info + ", "+location);
		} catch (HabitatServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Store window (as a bundle), inputs, and membership edges
	 * 
	 * @param windowToken
	 * @param inputList
	 *            -- all tuples in the window
	 * @param inputs
	 *            -- tokens -> tuples for all NEW tuples in the window
	 * @param location
	 */
	public void storeWindowAndInputs(ProvToken windowToken, List<ProvToken> inputList,
			ProvSpecifier location) {

		// storing window token
		try {
			prov.collection(getGraph(), new QualifiedName("p", windowToken.toString()), ProvSpecifierModel.from(location));
			// logger.info("node, window, "+windowToken + ", "+location);
		} catch (HabitatServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			// Add edges to all (new and old)
			for (ProvToken input : inputList) {
				prov.hadMember(getGraph(), new QualifiedName("p", windowToken.toString()),
						new QualifiedName("p", input.toString()));
				// logger.info("edge, partOf, "+inList.toString() + ", "+windowToken);
			}

		} catch (HabitatServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Store a tuple within a stream by its sequence
	 * 
	 * @param tupleToken
	 * @param input
	 * @param streamIndex
	 */
	public void storeTuple(ProvToken tupleToken, BasicTuple input, int streamIndex) {
		// storing input token
		try {
			prov.entity(getGraph(), new QualifiedName("p", tupleToken.toString()),
					getAttributes(input),
					ProvSpecifierModel.from(new ProvLocation("tupIdx", streamIndex)));
		} catch (HabitatServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Store a tuple within a stream by a specifier
	 * 
	 * @param tupleToken ID of the tuple
	 * @param input The tuple, to be stored
	 * @param location Where the tuple appeared in the stream
	 */
	public void storeTuple(ProvToken tupleToken, BasicTuple input, ProvSpecifier location) {

		// storing input token
		try {
			prov.entity(getGraph(), new QualifiedName("p", tupleToken.toString()),
					getAttributes(input),
					ProvSpecifierModel.from(location));
		} catch (HabitatServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Link activity, result, and window
	 * 
	 * @param activityToken The activity creating the result
	 * @param resultToken What we produced
	 * @param windowToken A token for the window of results
	 * @param start Time of execution
	 */
	public void storeLinks(ProvToken activityToken, ProvToken resultToken, ProvToken windowToken,
			Date start) {
		try {
			QualifiedName activity = new QualifiedName("p", activityToken.toString());
			QualifiedName input = new QualifiedName("p", windowToken.toString());
			QualifiedName output = new QualifiedName("p", resultToken.toString());
			QualifiedName activityId = new QualifiedName("p", activityToken.toString() + windowToken.toString());
			QualifiedName genId = new QualifiedName("p", resultToken.toString() + activityToken.toString());
			prov.used(getGraph(), activity, input, activityId, start);
			prov.wasGeneratedBy(getGraph(), output, activity, genId, start);

			prov.wasDerivedFrom(getGraph(), output, input, genId, activity, activityId);
		} catch (HabitatServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Store was-generated-by relationship / link
	 * 
	 * @param activityToken The activity that created the relationship
	 * @param tokens IDs for what we read as input (may have multiple operands)
	 * @param resultToken ID of what we we produced
	 * @param start Time of production
	 */
	public void storeLinks(ProvStringToken activityToken, Set<ProvToken> tokens, ProvToken resultToken, Date start) {
		try {
			QualifiedName activity = new QualifiedName("p", activityToken.toString());
			QualifiedName output = new QualifiedName("p", resultToken.toString());
			QualifiedName genId = new QualifiedName("p", resultToken.toString() + activityToken.toString());

			prov.wasGeneratedBy(getGraph(), output, activity, genId, start);

			for (ProvToken source: tokens) {
				QualifiedName input = new QualifiedName("p", source.toString());
				QualifiedName activityId = new QualifiedName("p", activityToken.toString() + source.toString());
				
				prov.used(getGraph(), activity, input, activityId, start);
				prov.wasDerivedFrom(getGraph(), output, input, genId, activity, activityId);
			}
		} catch (HabitatServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Annotate a node with metadata
	 * 
	 * @param sourceToken Unique ID for the element we are annotating
	 * @param annotToken Unique ID for the annotation
	 * @param qualifiedName Metadata field name
	 * @param content Content
	 */
	public void storeAnnotation(ProvToken sourceToken, ProvToken annotToken, QualifiedName qualifiedName,
			String content) {
		try {
			prov.annotateNode(
					getGraph(), 
					new QualifiedName("p", sourceToken.toString()),
					new QualifiedName("p", annotToken.toString()),
					qualifiedName, content); 
		} catch (HabitatServiceException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Take a tuple and convert it into a list of attributes to be
	 * encoded with the node in the provenance graph
	 *  
	 * @param tuple
	 * @return
	 */
	public List<Attribute> getAttributes(TupleWithSchema<String> tuple) {
		List<Attribute> ret = new ArrayList<Attribute>();
		
		for (int i = 0; i < tuple.size(); i++) {
			ret.add(new StringAttribute(new QualifiedName("n", tuple.getSchema().getKeyAt(i)), 
					tuple.getValueAt(i).toString()));
		}
		return ret;
	}
}
