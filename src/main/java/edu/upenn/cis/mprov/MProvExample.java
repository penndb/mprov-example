/************************************************
 * Copyright 2018 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.mprov;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.OptionalDouble;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mashape.unirest.http.exceptions.UnirestException;

import edu.umemphis.ccortex.RegisterStreams;
import edu.umemphis.ccortex.SourceDescriptor;
import edu.upenn.cis.db.habitat.core.type.BasicTuple;
import edu.upenn.cis.db.habitat.core.type.Field;
import edu.upenn.cis.db.habitat.core.type.StringField;
import edu.upenn.cis.db.habitat.core.webservice.HabitatServiceException;
import edu.upenn.cis.mprov.client.MProvConnection;
import edu.upenn.cis.mprov.streamoperations.CsvStreamSource;
import edu.upenn.cis.mprov.streamoperations.IStreamSource;
import edu.upenn.cis.mprov.streamoperations.WindowedAggregateOp;

/**
 * A simple streaming example program for MProv.
 * Reads some of the CCortex sample data files and "plays"
 * then as streams.  Propagates the streams down to the
 * next operator, etc.
 * 
 * @author ZacharyIves
 *
 */
public class MProvExample {
	static final String defaultUser = "sample";
	static final String defaultPassword = "default";
	
	static MProvConnection conn;
	
	public static void main(String args[]) {
		String user = defaultUser;
		String password = defaultPassword;
		if (args.length > 1) {
			user = args[0];
			password = args[1];
		}
		try {
			conn = new MProvConnection(user, password);
			
			System.out.println("Successfully connected to the MProv server");
			
			Map<String,SourceDescriptor> sources = RegisterStreams.sortSensors(Paths.get("./test-data"));
			
			System.out.println("Sensors: " + sources.keySet());
			
			
			/// Get the chest sensor
			IStreamSource source = getChestAccel(sources);
			source.initialize();
			
			/// Second operator in the stream
			WindowedAggregateOp<Double,OptionalDouble> windowOp = new WindowedAggregateOp<Double,OptionalDouble>("AVG", 5, 1, 
					"Accelerometer X", 
					(List<Double> var) -> var.stream().mapToDouble(Double::doubleValue).average(), OptionalDouble.class, Double.class,
					conn);
			
			
			List<BasicTuple> results = source.poll(1);
			while (!results.isEmpty()) {
				results = source.poll(1);
				
				for (BasicTuple tup: results) {
					System.out.println(tup.toString());
					windowOp.process(source.getName(), tup);
					
					List<BasicTuple> outputs = windowOp.poll(1);
					while (!outputs.isEmpty()) {
						for (BasicTuple t: outputs)
							System.out.println(t.toString());
						outputs = windowOp.poll(1);
					}
				}
			}
			
		} catch (HabitatServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnirestException e) {
			System.err.println("Is the Docker container running, and did you go to http://localhost:8088 and 'Sign up' a new user " + 
					defaultUser + "/" + defaultPassword + "?");
		} catch (JsonProcessingException e) {
			System.err.println("Unable to parse some of the JSON headers in the sample data");
		} catch (IOException e) {
			System.err.println("I/O error, did you run download.sh?");
		}
	}
	
	public static IStreamSource getChestAccel(Map<String,SourceDescriptor> sources) {
	    String accel = "ACCELEROMETER--org.md2k.autosenseble--AUTOSENSE_BLE--CHEST";
	    SourceDescriptor source = sources.get(accel);
	    source.setName("CHEST-ACC");
	    List<String> staticFields = new ArrayList<>();
	    staticFields.add("device");
	    
	    List<Field<?>> fieldValues = new ArrayList<>();
	    fieldValues.add(new StringField("chest1"));
	    return new CsvStreamSource(source.getName(), source, staticFields, fieldValues, conn);
	}
}
