import sys
from pennprov.connection.mprov_connection import MProvConnection
from mprov.ccortex.streams import RegisterStreams, SourceDescriptor
from mprov.streamoperators.csv_stream_source import CsvStreamSource
from mprov.streamoperators.windowed_agg import WindowedAggregateOp
from statistics import mean

default_user = 'YOUR_USERNAME' # sample
default_password = 'YOUR_PASSWORD' # default

def get_chest_accel(sensor_dict: dict, conn):
    accel = 'ACCELEROMETER--org.md2k.autosenseble--AUTOSENSE_BLE--CHEST'
    source = CsvStreamSource('CHEST-ACC', sensor_dict.get(accel), ['device'], ['chest1'], conn)
    return source


########################
user = default_user
password = default_password
if len(sys.argv) > 2:
    user = sys.argv[1]
    password = sys.argv[2]
# First step: establish a secure connection to the mprov server
conn = MProvConnection(user, password, None)
print("Successfully connected to the MProv server")

# For the sample code, we need to read data from the test-data
# directory (which must be preinstalled via download.sh) to
# replay sensor streams
sensor_dict = RegisterStreams.sort_sensors('../test-data')

print('Sensors:', sensor_dict.keys())

# The sample app works with chest accelerometer data
source = get_chest_accel(sensor_dict, conn)

# We take acceleromter data, do a sliding window of the last 5 readings,
# sliding by 1, and compute the mean value of Accelerometer X
agg = WindowedAggregateOp("AVG", 5, 1, "Accelerometer X", mean, conn)

# Now we start the stream processing part
source.initialize()

results = source.poll(1)
while results:
    if len(results) > 0:
        print(results[0])

    for r_tuple in results:
        #print(tuple)

        agg.process(source.name, r_tuple)
        outputs = agg.poll(1)
        while outputs and len(outputs) > 0:
            for t in outputs:
                print (t)
            outputs = agg.poll(1)

    results = source.poll(1)
